package com.wallington.cardview;

/**
 * Created by Wallington on 16/11/2018.
 */

public class Carta {

    private int frontImage;
    private int backImage;
    private Estat estat;

    public Carta(int front, int back) {
        this.frontImage = front;
        this.backImage = back;
        this.estat = Estat.BACK;
    }



    public enum  Estat {FIXED, BACK, FRONT}

    public int getImage() {
        int retorno = 0;
        switch (estat) {
            case FIXED:
                retorno = frontImage;
            break;
            case BACK:
                retorno = backImage;
            break;
            case FRONT:
                retorno = frontImage;
            break;
        }
        return retorno;
    }

    public void girar() {
        if (estat == Estat.BACK) {
            estat = Estat.FRONT;
        } else {
            estat = Estat.BACK;
        }
    }

    public Estat getEstat() {
        return estat;
    }

    public void setEstat(Estat estat) {
        this.estat = estat;
    }
}
