package com.wallington.cardview;

import android.content.Context;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Wallington on 16/11/2018.
 */


public class CartaAdapter extends RecyclerView.Adapter<CartaAdapter.CartaViewHolder> {
    private List<Carta> cartas;
    private Context context;


    public class CartaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        int contadorAbiertas = 0;
        Carta ultimaCarta;

        public ImageView imagen;

        public CartaViewHolder(View itemView) {
            super(itemView);
            imagen = (ImageView) itemView.findViewById(R.id.imagen);
            imagen.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            //TODO Aun no compara cartas
            Carta cartaSeleccionada = cartas.get(getAdapterPosition());
            if (cartaSeleccionada.getEstat() != Carta.Estat.FIXED)
                if (cartaSeleccionada != ultimaCarta) {
                    cartaSeleccionada.girar();
                    ultimaCarta = cartaSeleccionada;
                }
            notifyDataSetChanged();
        }
    }

    public CartaAdapter(List<Carta> cartas) {
        this.cartas = cartas;
        Collections.shuffle(cartas);
    }

    @Override
    public CartaAdapter.CartaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.carta, parent, false);
        return new CartaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CartaAdapter.CartaViewHolder holder, int position) {
        holder.imagen.setImageResource(cartas.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return cartas.size();
    }


}
