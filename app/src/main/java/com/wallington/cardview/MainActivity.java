package com.wallington.cardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ListAdapter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.wallington.cardview.Carta.Estat.FRONT;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Integer> cartas = new ArrayList<>();

        for (int i = 1; i < 49; i++) {
            cartas.add(getResources().getIdentifier("c" + i, "drawable", getPackageName()));
        }

        Collections.shuffle(cartas);

        List<Carta> listaAImprimir = new ArrayList<>();


        for (int i = 0; i < 6; i++) {
            listaAImprimir.add(new Carta(cartas.get(i), R.drawable.back));
            listaAImprimir.add(new Carta(cartas.get(i), R.drawable.back));
        }

        Collections.shuffle(listaAImprimir);


        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        lManager = new GridLayoutManager(this, 3);
        recycler.setLayoutManager(lManager);

        adapter = new CartaAdapter(listaAImprimir);
        recycler.setAdapter(adapter);
    }
}
